function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function insertSmileFaces(sporocilo) {
  sporocilo.html(sporocilo.html().replace(/;\)/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png">'));
  sporocilo.html(sporocilo.html().replace(/:\)/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png">'));
  sporocilo.html(sporocilo.html().replace(/\(y\)/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png">'));
  sporocilo.html(sporocilo.html().replace(/:\*/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png">'));
  sporocilo.html(sporocilo.html().replace(/:\(/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png">'));
  return sporocilo;
}

function procesirajVnosUporabnika(klepetApp, socket) { 
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
   // $('#sporocila').append(divElementEnostavniTekst(sporocilo));//original
    //$('#sporocila').append((sporocilo));//zjebe samo sebe;
    sporocilo = divElementEnostavniTekst(sporocilo);
    //sporocilo = insertSmileFaces(sporocilo);
    //sporocilo = '<div style="font-weight: bold">'+sporocilo+'</div>';
    //sporocilo.html(sporocilo.html().replace(/abc/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png">'));
    sporocilo = insertSmileFaces(sporocilo);
    $('#sporocila').append(sporocilo);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}


var socket = io.connect();
var mojVzdevek;
var mojKanal;

function popraviNaslov() {
  $('#kanal').text(mojVzdevek+" @ "+mojKanal);
}

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      mojVzdevek = rezultat.vzdevek;
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    popraviNaslov();
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    mojKanal = rezultat.kanal;
    //$('#kanal').text(rezultat.vzdevek+" @ "+rezultat.kanal);
    popraviNaslov();
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    //novElement = insertSmileFaces(novElement);
    //var novElement = '<div style="font-weight: bold">'+sporocilo.besedilo+'</div>';//zjebe use ostale
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});